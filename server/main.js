import { Meteor } from 'meteor/meteor';

import '../imports/startup/server';
import '../imports/api/forgotPassword.js';

Meteor.startup(() => {
  // code to run on server at startup
});
