import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

// Import to load these templates
import '../../ui/layouts/app-body.js';
import '../../ui/pages/login.js';
import '../../ui/pages/register.js';
import '../../ui/pages/user.js';
import '../../ui/pages/forgot-password.js';
import '../../ui/pages/not-found.js';

FlowRouter.route('/', {
  name: 'home',
  action() {
    if (Meteor.userId()) {
      console.log(Meteor.userId())
      BlazeLayout.render('App_body', { main: 'user' });
    } else {
      BlazeLayout.render('App_body', { main: 'login' });
    }
  },
});

FlowRouter.route('/register', {
  name: 'register',
  action() {
    BlazeLayout.render('App_body', { main: 'register' });
  },
});

FlowRouter.route('/forgotpassword', {
  name: 'forgotPassword',
  action() {
    BlazeLayout.render('App_body', { main: 'forgotPassword' });
  },
});

// the App_notFound template is used for unknown routes and missing lists
FlowRouter.notFound = {
  action() {
    BlazeLayout.render('App_body', { main: 'not-found' });
  },
};
