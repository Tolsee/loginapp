import { Meteor} from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';


Meteor.methods({
	'forgotpassword'(email) {
		var validEmail = /^[A-Z0-9\._%+-]+@[A-Z0-9\.-]+\.[A-Z]{2,}$/i;

		if (validEmail.test(email)) {
			Accounts.forgotPassword(email, function (err) {
				if (err) {
					throw new Meteor.Error('Provide valid email address!');
				}
			});
		}
	},
})