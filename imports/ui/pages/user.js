import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';

import { Template } from 'meteor/templating';

import './user.html';

Template.user.helpers({
	user: function(){
		return Meteor.user();
	}
});

Template.user.events({
	'click .logout'(event) {
		event.preventDefault();

		Meteor.logout(function(err) {
			if (err) {
				console.log("error", err);
			}
			FlowRouter.reload();
		})
	}
});