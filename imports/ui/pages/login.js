import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';

import { Template } from 'meteor/templating';


import './login.html';

Template.login.onRendered(function loginPageRendered() {
	jQuery('.js-validation-login').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },
            rules: {
                'login-username': {
                    required: true,
                    minlength: 3
                },
                'login-password': {
                    required: true,
                    minlength: 5
                }
            },
            messages: {
                'login-username': {
                    required: 'Please enter a username',
                    minlength: 'Your username must consist of at least 3 characters'
                },
                'login-password': {
                    required: 'Please provide a password',
                    minlength: 'Your password must be at least 5 characters long'
                }
            }
        });
})

Template.login.helpers({
	alert: function() {
		var success = FlowRouter.getQueryParams('success');
		var msg 	= FlowRouter.getQueryParams('msg');
		if (!msg) {
			return;
		}
		var alert = {}
		alert.class = success == true ? 'success' : 'warning';
		alert.message = msg;
		return alert;
	}
})

Template.login.events({
	'submit .login'(event) {
		event.preventDefault();

		var data = $('form').serializeArray().reduce(function(obj, item) {
			key = item.name.split('login-')[1];
		    obj[key] = item.value;
		    return obj;
		}, {});
		
		Meteor.loginWithPassword(data.username, data.password, function(err) {
			if (err) {
				$('.login-error').text(err.reason)
				FlowRouter.go('/');
			} else{
				if (!data['remember-me']) {
					// stops Accounts from logging you out due to token change
					Accounts._autoLoginEnabled = false;
					// remove login token from LocalStorage
					Accounts._unstoreLoginToken();
				}
				

				FlowRouter.reload();
			}

		})
	}
})

