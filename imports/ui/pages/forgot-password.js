import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import './forgot-password.html';

Template.forgotPassword.onRendered(function forgotPasswordRendered() {
	jQuery('.js-validation-reminder').validate({
        errorClass: 'help-block text-right animated fadeInDown',
        errorElement: 'div',
        errorPlacement: function(error, e) {
            jQuery(e).parents('.form-group > div').append(error);
        },
        highlight: function(e) {
            jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
            jQuery(e).closest('.help-block').remove();
        },
        success: function(e) {
            jQuery(e).closest('.form-group').removeClass('has-error');
            jQuery(e).closest('.help-block').remove();
        },
        rules: {
            'reminder-email': {
                required: true,
                email: true
            }
        },
        messages: {
            'reminder-email': {
                required: 'Please enter a valid email address'
            }
        }
    });
})

Template.forgotPassword.events({
	'submit .js-validation-reminder'(event) {
		event.preventDefault();

		var data = $('form').serializeArray().reduce(function(obj, item) {
			key = item.name.split('reminder-')[1];
		    obj[key] = item.value;
		    return obj;
		}, {});
		
		console.log(data);

		Meteor.call('forgotPassword', data);
	}
})