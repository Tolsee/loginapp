import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Accounts } from 'meteor/accounts-base';
import { ReactiveDict } from 'meteor/reactive-dict';


import './register.html';
Template.register.onCreated(function registerOnCreated() {
	this.state = new ReactiveDict()
});

Template.register.onRendered(function registerRendered() {
	jQuery('.js-validation-register').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },
            rules: {
                'register-username': {
                    required: true,
                    minlength: 3
                },
                'register-email': {
                    required: true,
                    email: true
                },
                'register-password': {
                    required: true,
                    minlength: 5
                },
                'register-password2': {
                    required: true,
                    equalTo: '#register-password'
                },
                'register-terms': {
                    required: true
                }
            },
            messages: {
                'register-username': {
                    required: 'Please enter a username',
                    minlength: 'Your username must consist of at least 3 characters'
                },
                'register-email': 'Please enter a valid email address',
                'register-password': {
                    required: 'Please provide a password',
                    minlength: 'Your password must be at least 5 characters long'
                },
                'register-password2': {
                    required: 'Please provide a password',
                    minlength: 'Your password must be at least 5 characters long',
                    equalTo: 'Please enter the same password as above'
                },
                'register-terms': 'You must agree to the service terms!'
            }
        });
})
Template.register.helpers({
	agree: function() {
		const instance = Template.instance();
		return instance.state.get('agree') == true ? 'checked' : '';
	}
})
Template.register.events({
	'submit .register'(event){
		event.preventDefault();

		var data = $('form').serializeArray().reduce(function(obj, item) {
			key = item.name.split('register-')[1];
		    obj[key] = item.value;
		    return obj;
		}, {});
		
		Accounts.createUser(data, function(err) {
			if (err) {
				$('.register-error').text(err.reason)
			} else {
				FlowRouter.go('/');
			}
		})
	},
	'click .btn-agree'(event, instance) {
		instance.state.set('agree', true);
	}
});